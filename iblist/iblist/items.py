# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class IblistItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
  title = scrapy.Field()
  date = scrapy.Field()
  bookType = scrapy.Field()
  author = scrapy.Field()
  summary = scrapy.Field()
  rating = scrapy.Field()
  genre = scrapy.Field()
  place = scrapy.Field()
  isbn = scrapy.Field()

    pass
