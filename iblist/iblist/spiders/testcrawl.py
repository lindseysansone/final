import re

def parse_torrent(html):
    title = ''
    bookType = ''
    author = ''
    summary = ''
    rating = ''
    genre = ''
    placeOfPublication = ''
    isbn = ''
    genres = []
    date = ''
    #Find title and date
    summaryRegex = re.compile('<i>Summary\n*.*\n*.*<div class="indent">')
    ratingRegex = re.compile("<b>[0-9]+\.[0-9]*<b>")
    anchorCloseRegex = re.compile("\</a\>")
    titleRegex = re.compile("class=\"big\".*\>")
    titleMatch = titleRegex.search(html)

    x = html.find('class="big"')
    y = html.find(">", x)
    z = html.find("<", y)
    print str(x) + ' '+ str(y) + ' ' + str(z)
    title = html[y+1:z]
    print title
    if x:
      titleMatchIndex = titleMatch.end()
      #titleEndMatch = anchorCloseRegex.search(html, titleMatchIndex)
      titleEndMatchIndex = html.find("</a>", titleMatchIndex)
      #title = html[titleMatchIndex:titleEndMatchIndex]
      #print title
      dateIndex =  title.find('(')
      endDateIndex = title.find(')')
      date = title[dateIndex+1:endDateIndex]
      #print date
      title = title[0:dateIndex]
      #print title
      #date = title[dateIndex+1:endDateIndex-1]
#Type
      typeIndex = html.find("</a>", z)
      typeIndex = typeIndex + 4
      typeEndIndex = html.find("<", typeIndex)
      bookType = html[typeIndex+2:typeEndIndex-1]
      
      #bookType = re.sub('[ | ] | \w', '', bookType)

      #Author
      byIndex = html.find('by', z)
      if byIndex:
        
        endAnchor = html.find('>', byIndex)
        endAuthor = html.find('</a', endAnchor)
        author = html[endAnchor+1:endAuthor]

      #Rating
      ratingIndex = html.find('<i>Rating:</i>')
      ratingMatch = ratingRegex.search(html, ratingIndex)
      if ratingMatch:
        rating = html[ratingMatch.start() + 3, ratingMatch.end() - 4]
      #Summary
      summaryMatch = summaryRegex.search(html, ratingIndex)
      if summaryMatch:
        summaryEndIndex = html.find('</div>', summaryMatch.end())
        summary = html[summaryMatch.end():summaryEndIndex]
        summary = re.sub('<?/p>', '', summary)
        #print summary
      #Genres
      genreIndex = html.find('<i>Genre:</i>', ratingIndex)
      anchorRegex = re.compile('<a href=".*">')
      while(1):
        #anchorMatch = anchorRegex.search(html, genreIndex)
        anchorMatchIndex = html.find('<a href="', genreIndex)
        anchorMatchClose = html.find('>', anchorMatchIndex)
        if anchorMatchIndex:
          breakIndex = html.find('<br', genreIndex)
          anchorEnd = html.find('</a>', anchorMatchClose)
          if breakIndex < anchorEnd:
            break
          else:
            genreIndex = anchorEnd
          genres.append(html[anchorMatchClose+1:anchorEnd])
        else:
          break

      #Place of Publication
      popIndex = html.find("<i>Place of publication")
      if popIndex != -1:
        endPlace = html.find('<br', popIndex)
        placeOfPublication = html[popIndex + 29 : endPlace]
 #ISBN
      isbnIndex = html.find("<i>ISBN:</i>")
      anchorMatch = anchorRegex.search(html, isbnIndex)
      if anchorMatch:
        anchorEnd = html.find("</a>", anchorMatch.end())
        if anchorEnd:
          isbn = html[anchorMatch.end():anchorEnd]

    #Image - book cover
    #imgIndex = html.find("<img src=")
    #imgIndex = html.find("<img src=", imgIndex) #find second img, first one is on header
    torrent = {}
    #torrent=TorrentItem()
    torrent['title'] = title
    torrent['date'] = date
    torrent['bookType'] = bookType
    torrent['author'] = author
    torrent['rating'] = rating
    torrent['summary'] = summary
    torrent['genre'] = genres
    #torrent['place'] = placeOfPublication
    torrent['isbn'] = isbn
    print torrent


html = '''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<head>

		<meta http-equiv="Content-Language" content="en" />
		<meta http-equiv="content-type" content="text/html; charset=ISO-8859-1" />
		<meta name="keywords" content="book, science, novel, booklist, short story, scifi, list, database, author, writer, science fiction, internet book list, archive, iblist.com, iblist.org, iblist.net, biography, writer, synopsis, best, listing,  bibliography, sci-fi, horror, humor, autobiography, romance, western, thriller, crime, mystery, murder" />
		<meta name="description" content="A comprehensive and easily accessible database of book information, including reviews and ratings. Find books by title, author, rating, or genre." />
		<meta name="author" content="IBList Group - iblistgroup@iblist.com" />
		<meta name="robots" content="all" />
		
		<!-- to correct the unsightly Flash of Unstyled Content. http://www.bluerobot.com/web/css/fouc.asp -->
		<script language="JavaScript">
		<!--
			function SymError()
			{
			  return true;
			}
			window.onerror = SymError;
		//-->
		</script>		
		
		<style type="text/css" title="defaultStyle">
			@import "http://www.iblist.com/css/site.css";
		</style>

		<link rel="Shortcut Icon" type="image/x-icon" href="http://www.iblist.com/images/favicon.ico" />

		<title>Internet Book List :: Book Information: Sounds of Silence</title>

		<base target="_self">

		<script language="JavaScript" type="text/JavaScript">
		<!--
		function trim(s) {
		  while (s.substring(0,1) == ' ') {
		    s = s.substring(1,s.length);
		  }
		  while (s.substring(s.length-1,s.length) == ' ') {
		    s = s.substring(0,s.length-1);
		  }
		  return s;
		}
		
		function mailto(a, s){
			t = window.open("mailto:"+a+"@iblist.com?subject="+s);
			t.window.close();
		}
		
		function show(e){
			if (document.getElementById && document.createTextNode){
				var element;
				element = document.getElementById(e);
				if (element.style.display == "none"){
					element.style.display = "block";
				}else if (element.style.display == "block"){
					element.style.display = "none";
				}else{
					element.style.display = "block";
				}
			}
		}		
		
	
		
		//-->
		</script>		
		
	</head>
	<body topmargin="5" leftmargin="5" marginwidth="5" marginheight="5" bgcolor="#FFFFFF">



<div id="top"><a href='http://www.iblist.com'><img src='http://www.iblist.com/images/iblist_logo.jpg' alt='The Internet Book List - Spread the word'></a></div>
<div id="content">
<!--navigation start-->
<div id="navigation">
	<!--search form-->
	<br />
	<center>
	<form action="http://www.iblist.com/search/search.php" method="get">
		<input class="formfield" type="text" name="item" size="14"><br />
		<input type="submit" name="submit" class="submit" value="Search" /><br />
		<input class="submit" name="advsearch" value="Adv Search" type="button" onClick="window.open('http://www.iblist.com/search/advanced_search.php')"/>
	</form>
	</center>
	<!--navigation main start-->
	<div class="navheader">Books</div>
	<ul>
		<li>by <a href="http://www.iblist.com/list.php?type=book">Title</a></li>
		<li>by <a href="http://www.iblist.com/list.php?type=author">Author</a></li>
		<li>by <a href="http://www.iblist.com/list.php?type=book&by=genre">Genre</a></li>
		<li>by <a href="http://www.iblist.com/list_by_rating.php?type=book">Rating</a></li>
	</ul>
	<div class="navheader">Authors</div>
	<ul>
		<li>by <a href="http://www.iblist.com/list.php?type=author">Name</a></li>
		<li>by <a href="http://www.iblist.com/list.php?type=author&by=genre">Genre</a></li>
		<li>by <a href="http://www.iblist.com/list_by_rating.php?type=author">Rating</a></li>
	</ul>
	<div class="navheader">Series</div>
	<ul>
		<li>by <a href="http://www.iblist.com/list.php?type=series">Name</a></li>
		<li>by <a href="http://www.iblist.com/list.php?type=series&by=genre">Genre</a></li>
		<li>by <a href="http://www.iblist.com/list_by_rating.php?type=series">Rating</a></li>
	</ul>
	<div class="navheader">IBList</div>
	<ul>		
		<li><a href="http://www.iblist.com/other/about.php">About</a></li>
		<li><a href="http://www.ibdof.com">Forums</a></li>
		<li><a href="http://www.iblist.com/other/donate.php">Donate</a></li>
		<li><a href="http://www.iblist.com/other/contact_the_iblist.php">Contact</a></li>
		<li><a href="http://mantis.iblist.com">Submit A Bug</a></li>
		<li><a href="http://www.iblist.com/other/contributors.php">Contributors</a></li>
		<li><a href="http://www.iblist.com/other/statistics.php">Statistics</a></li>
	</ul>
	<div class="navheader">User</div>
	<ul>
			<li><a href="http://www.iblist.com/users/register.php">Register</a></li>
		</ul>
	<!--navigation main end-->
	<!--bottom of navigation bar-->
	<div id="navfooter"></div>
	
<!--navigation end-->
</div>
<div id="login">
	<!--login?-->
		<form action="http://www.iblist.com/users/login.php" method="post" class="loginBox">
		<input type="text=" name="username" size="15" onClick="if(this.value=='username'){this.value='';}" onBlur="if(trim(this.value)==''){this.value='username';}" value="username" />&nbsp;		<input type="password" name="password" size="15" onClick="if(this.value=='password'){this.value='';}" onBlur="if(trim(this.value)==''){this.value='password';}" value="password" />&nbsp;		<input type=hidden name=redirectTo value="">
		<input type="submit" name="submit" class="submit" value="Login" />&nbsp;		<input type="submit" class="submit" name="new" value="New User" />&nbsp;	</form>
</div>
<div id="main">
<script type="text/javascript"><!--
google_ad_client = "pub-0972973213488733";
//728x90, created 11/26/07
google_ad_slot = "8397427439";
google_ad_width = 728;
google_ad_height = 90;
//--></script>
<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>
<br />
		


<br />
<h2>Book Information: Sounds of Silence</h2>
<div class="boxbody">
	<table align="center" class="main" width="100%">
		<tr>
		<td class="content">
		<table border="0" cellpadding="10" cellspacing="0" width="100%">
		<tr>
			<td class="content" valign="top" width="100%">
			<table align="center" valign="top" border="0" class="main" width="100%"><tr><td><b><a class="big" href="http://www.iblist.com/book70207.htm">Sounds of Silence (2005)</a> [Novel]</b><br />

					by <a href="http://www.iblist.com/author22122.htm">Elizabeth  White</a
			<p>
			<i>Rating:</i> <i>No votes</i> (<a href="http://www.iblist.com/submit/add_book_rating.php?id=70207">Rate!</a>)<br />
			<i>Reviews:</i> None (<a href="http://www.iblist.com/list_reviews.php?id=70207">show them</a>) <a href="http://www.iblist.com/submit/add_book_review.php?id=70207">Review!</a><br />
			
					<p><i>Series:</i> <a href="series4549.htm">Love Inspired Suspense</a><br />
					<i>Part:</i> 11
</p>
					<p><i>Series:</i> <a href="series5317.htm">Texas Gatekeepers</a><br />
					<i>Part:</i> 2
</p>
</td><td>
			</td></tr></table>
			<p><i>Summary
			 (From the publisher):</i><br />
				<div class="indent">Border Patrol agent Eli Carmichael knew the deaf child he'd found outside a Mexican orphanage was harboring a dark secret -- she was carrying a bloodstained knife and was clearly traumatized. To keep her safe, he turned to trusted neighbor Isabel Valenzuela. A sense of duty had kept Eli close to his fellow agent's widow and her young son over the past year, and now Eli was spending more time with Isabel and the kids, trying to determine exactly what the girl had seen. Under Isabel's gentle care, the child began to open up. But the killers were close by, and determined to silence the girl forever...</p></div>
						<p>
							<i>Original title:</i> Sounds of Silence<br />
								<i>Original languages:</i>
					English				<br />
						
			<br /><i>Quotes:</i> 
			<p>
					<i>Genre:</i>  <a href="http://www.iblist.com/list.php?type=book&by=genre&genre=1">Fiction</a>&rarr; <a href="http://www.iblist.com/list.php?type=book&by=genre&genre=71">Romance</a>&rarr; <a href="http://www.iblist.com/list.php?type=book&by=genre&genre=73">Romantic-Suspense</a><br />
<!--<br /><i>No related works were found in our database.</i>-->			</td>
		</tr>
		</table>
		</td>
		</tr>
	</table>
</div>

 
<h2><a name="E60432"></a>Edition #1: Sounds of Silence</h2>
<div class="boxbody">
	<table align="center" border="0" class="main" width="100%">
		<tr><td class="content" colspan="4" width="100%">
		<table border="0" cellpadding="10" cellspacing="0" width="100%"><tr>
			<td class="content" width="50%"><a href="http://www.iblist.com/book70207.htm#E60432">Sounds of Silence</a> (2005)			</td>
			<!-- EXPRESSION RATING --> 			
			</td>
		</tr></table>
		</td></tr>
<tr valign="top"><td colspan="4">
</td></tr>
		<tr valign="top"><td colspan="4">
		<i>Edition Details:</i><br /><p></p>		</td></tr><tr valign="top">
		<td width="30%">
		<i>Language:</i> English<br /><br />
		
				   		 		</ br>
		</td>
		<td width="50%">
				</td>
	<td colspan="2" ><table><tr><td class="textborder" bgcolor="#eeeeee">
			<b><a href="http://www.amazon.com/gp/redirect.html?link_code=ur2&tag=internetbookl-20&camp=1789&creative=9325&location=/gp/search%3F%26index=books%26keywords=Sounds%20of%20Silence%20">Search at Amazon!</a><br /><br />
			<a href="http://www.powells.com/cgi-bin/partner?partner_id=28411&cgi=search/search/&searchtype=kw&searchfor=Sounds%20of%20Silence%20">Search at Powells!</a><br /><br />
			<a href="http://clickserve.cc-dt.com/link/tplclick?lid=41000000024289215&pubid=21000000000160834&redirect=http%3A%2F%2Fwww.abebooks.com%2Fservlet%2FBookSearchPL%3Fph%3D2%26tn%3DSounds%20of%20Silence%20">Search at AbeBooks!</a></b></td></tr></table></td>
		</tr>



		<tr><td class="content" colspan="4"><br /><br /><b>Manifested in:</b></td></tr>		<tr>
		<td class="content" width="20%"><!-- Should really be doing this sort of thing with CSS layout -->
		<div style="width: 85px; background: #dfce9e; border: 1px solid black; padding: 3px;"><!-- height: 120px; -->
		<img src="thumbs/covers/70646.jpg" width="85" alt="cover" border="1" /><br /><a href="imageviewer.php?id=71904&type=book&return=70207" target="_new">View all 1 Images</a>		</div>
	





		</td>
		<td class="content" width="60%" colspan="2">
			<p><a name="man71904"></a>Sounds of Silenc (December  1, 2009)</p><p>
			 
			<i>Format:</i> E-book<br /> 

			<i>Place of publication:</i> New York<br />			<i>Publisher:</i> <a href="http://www.iblist.com/publisher.php?id=87">Harlequin</a>
<br />
			<i>ISBN:</i> <a href="http://www.amazon.com/gp/redirect.html?link_code=ur2&tag=internetbookl-20&camp=1789&creative=9325&location=/gp/search%3F%26index=books%26keywords=9781426851315%20" target="_new">9781426851315</a><br /> 						<i>Pages:</i> 224<br /> 	</td>
		<td class="content" width="20%"><a href="http://www.iblist.com/library/add_to_list.php?id=71904&type=1&rid=70207">Add to <i>my library</i></a></td>
		</tr>	
				<tr>
		<td class="content" width="20%"><!-- Should really be doing this sort of thing with CSS layout -->
		<div style="width: 85px; background: #dfce9e; border: 1px solid black; padding: 3px;"><!-- height: 120px; -->
		<img src="thumbs/covers/70645.jpg" width="85" alt="cover" border="1" /><br /><a href="imageviewer.php?id=71903&type=book&return=70207" target="_new">View all 1 Images</a>		</div>
	





		</td>
		<td class="content" width="60%" colspan="2">
			<p><a name="man71903"></a>Sounds of Silence (November  29, 2005)</p><p>
			 
			<i>Format:</i> Paperback<br /> 

			<i>Place of publication:</i> New York<br />			<i>Publisher:</i> <a href="http://www.iblist.com/publisher.php?id=87">Harlequin</a>
<br />
			<i>ISBN:</i> <a href="http://www.amazon.com/gp/redirect.html?link_code=ur2&tag=internetbookl-20&camp=1789&creative=9325&location=/gp/search%3F%26index=books%26keywords=9780373442270%20" target="_new">9780373442270</a><br /> 			<i>Dimensions:</i> 4.3 x 6.3 x 0.9 in<br /> 		<i>Pages:</i> 256<br /> 					</td>
		<td class="content" width="20%"><a href="http://www.iblist.com/library/add_to_list.php?id=71903&type=1&rid=70207">Add to <i>my library</i></a></td>
		</tr>	
		</table></div class="boxbody">


<!--Begin administrative section-->

</div><!--end id=main div content-->

</div id="main">
<br /> <br /> <br /><div style="text-align: center; clear: both;">&copy;<a href="#" onClick="mailto('steven','IBList');">Steven Jeffery</a> / IBList.com, 2012
<br /> <a href="http://www.iblist.com/other/terms.php">Terms of Use</a></a>
<br style="clear: both;" />



</div id="content">

</body>

</html>
2015-04-17 03:52:36+0000 [iblist] DEBUG: Scraped from <200 http://www.iblist.com/book70207.htm>
	{'author': '',
	 'bookType': ')',
	 'date': '<a href="http://www.iblist.com/submit/add_book_rating.php?id=70207">Rat',
	 'genre': [],
	 'isbn': '',
	 'rating': '',
	 'summary': '',
	 'title': 'Transition'}
'''


parse_torrent(html)
