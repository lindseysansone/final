import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
import re


class TorrentItem(scrapy.Item):
  title = scrapy.Field()
  date = scrapy.Field()
  bookType = scrapy.Field()
  author = scrapy.Field()
  summary = scrapy.Field()
  rating = scrapy.Field()
  genre = scrapy.Field()
  place = scrapy.Field()
  isbn = scrapy.Field()

class IBLSpider(CrawlSpider):
  name="iblist"
  allowed_domains = ['iblist.com']
  start_urls = ['http://www.iblist.com']
  rules = [Rule(LinkExtractor(allow=['/book\d+\.htm']), 'parse_torrent')]
  
  def parse_torrent(self, response):
    html =  response.body
    print html
    title = ''
    bookType = ''
    author = ''
    summary = ''
    rating = ''
    genre = ''
    placeOfPublication = ''
    isbn = ''
    genres = []
    date = ''
    #Find title and date
    summaryRegex = re.compile("<i>Summary:.*<div class=\"indent\">")
    ratingRegex = re.compile("<b>[0-9]+\.[0-9]*<b>")
    anchorCloseRegex = re.compile("\</a\>")
    titleRegex = re.compile("class=\"big\".*\>")
    titleMatch = titleRegex.search(html)

    x = html.find("class='big'")
    y = html.find(">", x)
    z = html.find("<", y)
    title = html[y:z]
    if x:
      titleMatchIndex = titleMatch.end()
      #titleEndMatch = anchorCloseRegex.search(html, titleMatchIndex)
      titleEndMatchIndex = html.find("</a>", titleMatchIndex)
      title = html[titleMatchIndex:titleEndMatchIndex]
      dateIndex =  title.find('(')
      endDateIndex = title.find(')')
      date = title[dateIndex+1:endDateIndex-1]
      title = title[0:dateIndex]
      title = html[45:55] 
      #Type
      typeIndex = html.find("</a>", titleEndMatchIndex)
      typeIndex = typeIndex + 4
      typeEndIndex = html.find("<", typeIndex)
      bookType = html[typeIndex:typeEndIndex]
      #bookType = re.sub('[ | ] | \w', '', bookType)

      #Author
      byIndex = html.find('by', titleMatchIndex)
      if byIndex:
        endAnchor = html.find('>', byIndex)
        endAuthor = html.find('</a>')
        author = html[endAnchor:endAuthor]

      #Rating
      ratingIndex = html.find('<i>Rating:</i>', typeIndex)
      ratingMatch = ratingRegex.search(html, ratingIndex)
      if ratingMatch:
        rating = html[ratingMatch.start() + 3, ratingMatch.end() - 4]

      #Summary
      summaryMatch = summaryRegex.search(html, ratingIndex)
      if summaryMatch:
        summaryEndIndex = find('</div>', summaryMatch.end())
        summary = html[summaryMatch.end():summaryEnd]
        summary = re.sub('<?/p>', '', summary)

      #Genres
      genreIndex = html.find('<i>Genre:</i>', ratingIndex)
      anchorRegex = re.compile("<a href='.*'>")
      while(1):
        anchorMatch = anchorRegex.search(html, genreIndex)
        if anchorMatch:
          breakIndex = html.find('<br>', genreIndex)
          anchorEnd = html.find('</a>', anchorMatch.end())
          if breakIndex < anchorEnd:
            break
          genres.append(html[anchorMatch.end():anchorEnd])
        else:
          break

      #Place of Publication
      popIndex = html.find("<i>Place of Publication</i>")
      if popIndex:
        placeOfPublication = html[popIndex + 27 : html.find('<br>', popIndex)]

    
      #ISBN
      isbnIndex = html.find("<i>ISBN:</i>")
      anchorMatch = anchorRegex.search(html, isbnIndex)
      if anchorMatch:
        anchorEnd = html.find("</a>", anchorMatch.end())
        if anchorEnd:
          isbn = html[anchorMatch.end():anchorEnd]

    #Image - book cover
    #imgIndex = html.find("<img src=")
    #imgIndex = html.find("<img src=", imgIndex) #find second img, first one is on header

    torrent=TorrentItem()
    torrent['title'] = title
    torrent['date'] = date
    torrent['bookType'] = bookType
    torrent['author'] = author
    torrent['rating'] = rating
    torrent['summary'] = summary
    torrent['genre'] = genres
    #torrent['place'] = placeOfPublication
    torrent['isbn'] = isbn
    return torrent 
