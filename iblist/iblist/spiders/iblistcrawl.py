import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor
import re


class IBLSpider(CrawlSpider):
  name="iblist"
  allowed_domains = ['iblist.com']
  start_urls = ['http://www.iblist.com']
  rules = [Rule(LinkExtractor(allow=['/book\d+\.htm']), 'parse')]
  
  def parse(self, response):
    html = response.body
    title = ''
    bookType = ''
    author = ''
    summary = ''
    rating = ''
    genre = ''
    placeOfPublication = ''
    isbn = ''
    genres = []
    date = ''
    #Find title and date
    summaryRegex = re.compile('<i>Summary\n*.*\n*.*<div class="indent">')
    ratingRegex = re.compile("<b>[0-9]+\.[0-9]*<b>")
    anchorCloseRegex = re.compile("\</a\>")
    titleRegex = re.compile("class=\"big\".*\>")
    titleMatch = titleRegex.search(html)

    x = html.find('class="big"')
    y = html.find(">", x)
    z = html.find("<", y)
    title = html[y+1:z]
    if x:
      titleMatchIndex = titleMatch.end()
      #titleEndMatch = anchorCloseRegex.search(html, titleMatchIndex)
      titleEndMatchIndex = html.find("</a>", titleMatchIndex)
      #title = html[titleMatchIndex:titleEndMatchIndex]
      #print title
      dateIndex =  title.find('(')
      endDateIndex = title.find(')')
      date = title[dateIndex+1:endDateIndex]
      #print date
      title = title[0:dateIndex]
      #print title
      #date = title[dateIndex+1:endDateIndex-1]
#Type
      typeIndex = html.find("</a>", z)
      typeIndex = typeIndex + 4
      typeEndIndex = html.find("<", typeIndex)
      bookType = html[typeIndex+2:typeEndIndex-1]

    #Author
      byIndex = html.find('by', z)
      if byIndex:

        endAnchor = html.find('>', byIndex)
        endAuthor = html.find('</a', endAnchor)
        author = html[endAnchor+1:endAuthor]

      #Rating
      ratingIndex = html.find('<i>Rating:</i>')
      ratingMatch = ratingRegex.search(html, ratingIndex)
      if ratingMatch:
        rating = html[ratingMatch.start() + 3, ratingMatch.end() - 4]
      #Summary
      summaryMatch = summaryRegex.search(html, ratingIndex)
      if summaryMatch:
        summaryEndIndex = html.find('</div>', summaryMatch.end())
        summary = html[summaryMatch.end():summaryEndIndex]
        summary = re.sub('<?/p>', '', summary)
        #print summary
      #Genres
      genreIndex = html.find('<i>Genre:</i>', ratingIndex)
      anchorRegex = re.compile('<a href=".*">')
      while(1):
        #anchorMatch = anchorRegex.search(html, genreIndex)
        anchorMatchIndex = html.find('<a href="', genreIndex)
        anchorMatchClose = html.find('>', anchorMatchIndex)
        if anchorMatchIndex:
          breakIndex = html.find('<br', genreIndex)
          anchorEnd = html.find('</a>', anchorMatchClose)
          if breakIndex < anchorEnd:
            break
          else:
            genreIndex = anchorEnd
          genres.append(html[anchorMatchClose+1:anchorEnd])
        else:
          break

      #Place of Publication
      popIndex = html.find("<i>Place of publication")
      if popIndex != -1:
        endPlace = html.find('<br', popIndex)
        placeOfPublication = html[popIndex + 29 : endPlace]
 #ISBN
      isbnIndex = html.find("<i>ISBN:</i>")
      anchorMatch = anchorRegex.search(html, isbnIndex)
      if anchorMatch:
        anchorEnd = html.find("</a>", anchorMatch.end())
        if anchorEnd:
          isbn = html[anchorMatch.end():anchorEnd]
    #Image - book cover
    #imgIndex = html.find("<img src=")
    #imgIndex = html.find("<img src=", imgIndex) #find second img, first one is on header

    torrent=IblistItem()
    torrent['title'] = title
    torrent['date'] = date
    torrent['bookType'] = bookType
    torrent['author'] = author
    torrent['rating'] = rating
    torrent['summary'] = summary
    torrent['genre'] = genres
    torrent['place'] = placeOfPublication
    torrent['isbn'] = isbn
    return torrent 
