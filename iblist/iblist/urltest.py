#Authors: Alexander Hathaway & Lindsey Sansone
#Crawl IBList.com for book information
#insert data into database

import urllib2
import re

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'shelfdata'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, use_unicode=True)
cursor = cnx.cursor()

# Switch to Database
useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

def Crawl():
  for book_no in range(1, 70217):
    url = 'http://www.iblist.com/book' + str(book_no) + '.htm'
    try: 
      response = urllib2.urlopen(url)
      html = response.read()
    except:
      continue

    title = ''
    bookType = ''
    author = ''
    summary = ''
    rating = ''
    genre = ''
    placeOfPublication = ''
    isbn = ''
    genres = []
    date = ''
    imgUrl = ''
    #Find title and date
    summaryRegex = re.compile('<i>Summary\n*.*\n*.*<div class="indent">')
    ratingRegex = re.compile("<b>[0-9]+\.[0-9]*</b>")
    anchorCloseRegex = re.compile("\</a\>")
    titleRegex = re.compile("class=\"big\".*\>")
    titleMatch = titleRegex.search(html)

    x = html.find('class="big"')
    y = html.find(">", x)
    z = html.find("<", y)
    title = html[y+1:z]
    if x:
      titleMatchIndex = titleMatch.end()
      #titleEndMatch = anchorCloseRegex.search(html, titleMatchIndex)
      titleEndMatchIndex = html.find("</a>", titleMatchIndex)
      #title = html[titleMatchIndex:titleEndMatchIndex]
      #print title
      dateIndex =  title.find('(')
      endDateIndex = title.find(')')
      date = title[dateIndex+1:endDateIndex]
      #print date
      title = title[0:dateIndex]
      #print title
      #date = title[dateIndex+1:endDateIndex-1]
      #Type
      typeIndex = html.find("</a>", z)
      typeIndex = typeIndex + 4
      typeEndIndex = html.find("<", typeIndex)
      bookType = html[typeIndex+2:typeEndIndex-1]

    #Author
      byIndex = html.find('by', z)
      if byIndex:

        endAnchor = html.find('>', byIndex)
        endAuthor = html.find('</a', endAnchor)
        author = html[endAnchor+1:endAuthor]

      #Rating
      ratingIndex = html.find('<i>Rating:</i>')
      ratingMatch = ratingRegex.search(html, ratingIndex)
      if ratingMatch:
        rating = html[ratingMatch.start() + 3: ratingMatch.end() - 4]
      #Summary
      summaryMatch = summaryRegex.search(html, ratingIndex)
      if summaryMatch:
        summaryEndIndex = html.find('</div>', summaryMatch.end())
        summary = html[summaryMatch.end():summaryEndIndex]
        summary = re.sub('<?/p>', '', summary)
        #print summary
      #Genres
      genreIndex = html.find('<i>Genre:</i>', ratingIndex)
      anchorRegex = re.compile('<a href=".*">')
      while(1):
        #anchorMatch = anchorRegex.search(html, genreIndex)
        anchorMatchIndex = html.find('<a href="', genreIndex)
        anchorMatchClose = html.find('>', anchorMatchIndex)
        if anchorMatchIndex:
          breakIndex = html.find('<br', genreIndex)
          anchorEnd = html.find('</a>', anchorMatchClose)
          if breakIndex < anchorEnd:
            break
          else:
            genreIndex = anchorEnd
          genres.append(html[anchorMatchClose+1:anchorEnd])
        else:
          break

      #Place of Publication
      popIndex = html.find("<i>Place of publication")
      if popIndex != -1:
        endPlace = html.find('<br', popIndex)
        placeOfPublication = html[popIndex + 29 : endPlace]
 #ISBN
      isbnIndex = html.find("<i>ISBN:</i>")
      anchorMatch = anchorRegex.search(html, isbnIndex)
      if anchorMatch:
        anchorEnd = html.find("</a>", anchorMatch.end())
        if anchorEnd:
          isbn = html[anchorMatch.end():anchorEnd]
    #Image - book cover
    imgIndex = html.find('<img src="thumbs/covers/') #find second img, first one is on header
    if imgIndex:
       imgEndIndex = html.find('"', imgIndex + 12)
       imgUrl = '<img src="thumbs/covers/' + html[imgIndex + 24: imgEndIndex]
    inputBook = {}
    inputBook['title'] = title
    inputBook['date'] = date
    inputBook['type'] = bookType
    inputBook['author'] = author
    inputBook['rating'] = rating
    inputBook['summary'] = summary
    inputBook['genre'] = genres
    inputBook['place'] = placeOfPublication
    inputBook['isbn'] = isbn 
    inputBook['imgUrl'] = imgUrl

    insertBookQuery = ("INSERT INTO Books (title, date, type, author, rating, summary, place, isbn, imgUrl) VALUES (%(title)s, %(date)s, %(type)s, %(author)s, %(rating)s, %(summary)s, %(place)s, %(isbn)s, %(imgUrl)s)")
    cursor.execute(addRestaruant, inputBook)

    for genre in genres:
      insertGenreQuery = ("INSERT INTO Genres (book_id, genre) VALUES (%s(book_id)s, %(genre)s)")
      inputGenre = {'book_id':



Crawl()
