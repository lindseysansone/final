#Authors: Alexander Hathaway & Lindsey Sansone
#Crawl IBList.com for book information
#insert data into database

import urllib2
import re

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'shelfdata'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, use_unicode=True)
cursor = cnx.cursor()

# Switch to Database
useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

def Crawl():
  for book_no in range(51, 1000):
    url = 'http://www.iblist.com/book' + str(book_no) + '.htm'
    try: 
      response = urllib2.urlopen(url)
      html = response.read()
    except:
      continue
    if html.find("Error: No book found.") != -1:
      continue
    title = ''
    bookType = ''
    author = ''
    summary = ''
    rating = ''
    genre = ''
    placeOfPublication = ''
    isbn = ''
    genres = []
    date = ''
    imgUrl = ''
    #Find title and date
    summaryRegex = re.compile('<i>Summary\n*.*\n*.*<div class="indent">')
    ratingRegex = re.compile("<b>[0-9]+\.[0-9]*</b>")
    anchorCloseRegex = re.compile("\</a\>")
    titleRegex = re.compile("class=\"big\".*\>")
    titleMatch = titleRegex.search(html)

    x = html.find('class="big"')
    y = html.find(">", x)
    z = html.find("<", y)
    title = html[y+1:z]
    if x:
      #titleMatchIndex = titleMatch.end()
      #titleEndMatch = anchorCloseRegex.search(html, titleMatchIndex)
      #titleEndMatchIndex = html.find("</a>", titleMatchIndex)
      #title = html[titleMatchIndex:titleEndMatchIndex]
      #print title
      dateIndex =  title.find('(')
      endDateIndex = title.find(')')
      date = title[dateIndex+1:endDateIndex]
      #print date
      title = title[0:dateIndex-1]
      #print title
      #date = title[dateIndex+1:endDateIndex-1]
      #Type
      typeIndex = html.find("</a>", z)
      typeIndex = typeIndex + 4
      typeEndIndex = html.find("<", typeIndex)
      bookType = html[typeIndex+2:typeEndIndex-1]
    #Author
      byIndex = html.find('by', z)
      if byIndex:

        endAnchor = html.find('>', byIndex)
        endAuthor = html.find('</a', endAnchor)
        author = html[endAnchor+1:endAuthor]

      #Rating
      ratingIndex = html.find('<i>Rating:</i>')
      ratingMatch = ratingRegex.search(html, ratingIndex)
      if ratingMatch:
        rating = html[ratingMatch.start() + 3: ratingMatch.end() - 4]
      #Summary
      summaryMatch = summaryRegex.search(html, ratingIndex)
      if summaryMatch:
        summaryEndIndex = html.find('</div>', summaryMatch.end())
        summary = html[summaryMatch.end():summaryEndIndex]
        summary = re.sub('<?/p>', '', summary)
        #print summary
      #Genres
      genreIndex = html.find('<i>Genre:</i>', ratingIndex)
      anchorRegex = re.compile('<a href=".*">')
      for j in range(0, 5):
        #anchorMatch = anchorRegex.search(html, genreIndex)
        anchorMatchIndex = html.find('<a href="', genreIndex)
        anchorMatchClose = html.find('>', anchorMatchIndex)
        if anchorMatchIndex:
          breakIndex = html.find('<br', genreIndex)
          anchorEnd = html.find('</a>', anchorMatchClose)
          if breakIndex < anchorEnd:
            break
          else:
            genreIndex = anchorEnd
          newGenre = html[anchorMatchClose+1:anchorEnd]
          if any(newGenre in g for g in genres):
            break
          genres.append(newGenre)
        else:
          break

      #Place of Publication
      popIndex = html.find("<i>Place of publication")
      if popIndex != -1:
        endPlace = html.find('<br', popIndex)
        placeOfPublication = html[popIndex + 29 : endPlace]
 #ISBN
      isbnIndex = html.find("<i>ISBN:</i>")
      #anchorMatch = anchorRegex.search(html, isbnIndex)
      
      if isbnIndex:
        anchorIndex = html.find("<a href=", isbnIndex)
        anchorEnd = html.find(">", anchorIndex)
        anchorClose = html.find("</a", anchorEnd)
        isbn = html[anchorEnd+1:anchorClose]
    #Image - book cover
    imgIndex = html.find('<img src="thumbs/covers/') #find second img, first one is on header
    if imgIndex:
       imgEndIndex = html.find('"', imgIndex + 12)
       imgUrl = 'http://www.iblist.com/thumbs/covers/' + html[imgIndex + 24: imgEndIndex]
    
    inputBook = {}
    inputBook['title'] = title
    inputBook['date'] = date
    inputBook['type'] = bookType
    inputBook['author'] = author
    inputBook['rating'] = rating
    inputBook['summary'] = summary
    #inputBook['genre'] = genres
    inputBook['place'] = placeOfPublication
    inputBook['isbn'] = isbn 
    inputBook['imgUrl'] = imgUrl
    #print inputBook
    insertBookQuery = ("INSERT INTO Books (title, date, type, author, rating, summary, place, isbn, imgUrl) VALUES (%(title)s, %(date)s, %(type)s, %(author)s, %(rating)s, %(summary)s, %(place)s, %(isbn)s, %(imgUrl)s)")
    cursor.execute(insertBookQuery, inputBook)
    book_id = cursor.lastrowid
    #print str(book_id)
    #print genres    
    for genre in genres:
      genreDict = {"book_id": book_id, "genre": genre}
      insertBookGenreQuery = ("INSERT INTO Genres (book_id, genre) VALUES (%(book_id)s, %(genre)s)")
      cursor.execute(insertBookGenreQuery, genreDict)

    print book_id

Crawl()

#####################################################
# Commit the data and close the connection to MySQL #
#####################################################
cnx.commit()
cnx.close()
