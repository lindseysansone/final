# Authors : Lindsey Sansone & Alexander Hathaway 
# Created on : April 12, 2014
# Modified on : April 12, 2014

## Use this script to create the database tables
## Note : this script does not populate the tables with any data

import mysql.connector

#Define database variables
DATABASE_USER = 'root'
DATABASE_HOST = '127.0.0.1'
DATABASE_NAME = 'shelfdata'

#Create connection to MySQL
cnx = mysql.connector.connect(user=DATABASE_USER, host=DATABASE_HOST, use_unicode=True)
cursor = cnx.cursor()

####################################
# Create Database if doesn't exist #
####################################

createDB = (("CREATE DATABASE IF NOT EXISTS %s DEFAULT CHARACTER SET utf8mb4") % (DATABASE_NAME))
cursor.execute(createDB)

# Switch to Database
useDB = (("USE %s") % (DATABASE_NAME))
cursor.execute(useDB)

#########################
# Drop all tables first #
#########################
dropTableQuery = ("DROP TABLE IF EXISTS Images")
cursor.execute(dropTableQuery)

dropTableQuery = ("DROP TABLE IF EXISTS UserGenres");
cursor.execute(dropTableQuery)

dropTableQuery = ("DROP TABLE IF EXISTS Genres")
cursor.execute(dropTableQuery)

dropTableQuery = ("DROP TABLE IF EXISTS UserLibrary")
cursor.execute(dropTableQuery)

dropTableQuery = ("DROP TABLE IF EXISTS UserRatings")
cursor.execute(dropTableQuery)

dropTableQuery = ("DROP TABLE IF EXISTS Users")
cursor.execute(dropTableQuery)

dropTableQuery = ("DROP TABLE IF EXISTS Books")
cursor.execute(dropTableQuery)


#################
# Create Tables #
#################

#Users
createTableQuery = ("CREATE TABLE Users ("
	"username VARCHAR(45) NOT NULL,"
	"password VARCHAR(120) CHARACTER SET utf8mb4 NOT NULL,"
	"email VARCHAR(45) CHARACTER SET utf8mb4 NOT NULL,"
	"UNIQUE KEY (email) USING BTREE,"
	"PRIMARY KEY (username))")
cursor.execute(createTableQuery)

#Books
createTableQuery = ("CREATE TABLE Books ("
	"book_id INT NOT NULL AUTO_INCREMENT,"
	"title VARCHAR(45) CHARACTER SET utf8mb4 NOT NULL,"
	"type VARCHAR(45) CHARACTER SET utf8mb4,"
	"author VARCHAR(45) CHARACTER SET utf8mb4 NOT NULL,"
	"rating FLOAT,"
        "date INT,"
	"summary VARCHAR(500) CHARACTER SET utf8mb4,"
	"place VARCHAR(45) CHARACTER SET utf8mb4,"
	"isbn VARCHAR(45) CHARACTER SET utf8mb4,"
        "imgUrl VARCHAR(45) CHARACTER SET utf8mb4,"
	"PRIMARY KEY (book_id))")
cursor.execute(createTableQuery)

#Genres
createTableQuery = ("CREATE TABLE Genres ("
	"genre VARCHAR(45) CHARACTER SET utf8mb4 NOT NULL,"
	"book_id INT NOT NULL,"
	"FOREIGN KEY (book_id) REFERENCES Books(book_id) on delete cascade,"
	"PRIMARY KEY (book_id,genre))")
cursor.execute(createTableQuery)

#UserRatings
createTableQuery = ("CREATE TABLE UserRatings ("
	"rating FLOAT NOT NULL,"
	"review VARCHAR(500) CHARACTER SET utf8mb4,"
	"book_id INT NOT NULL,"
	"username VARCHAR(45) CHARACTER SET utf8mb4 NOT NULL,"
	"FOREIGN KEY (book_id) REFERENCES Books(book_id) on delete cascade,"
	"FOREIGN KEY (username) REFERENCES Users(username) on delete cascade,"
	"PRIMARY KEY (book_id,username))")
cursor.execute(createTableQuery) 

#UserLibrary
createTableQuery = ("CREATE TABLE UserLibrary ("
	"username VARCHAR(45) CHARACTER SET utf8mb4 NOT NULL,"
	"book_id INT NOT NULL,"
	"status ENUM('will_read','reading','read') NOT NULL,"
	"FOREIGN KEY (book_id) REFERENCES Books(book_id) on delete cascade,"
	"FOREIGN KEY (username) REFERENCES Users(username) on delete cascade,"
	"PRIMARY KEY (book_id,username))")
cursor.execute(createTableQuery) 


#UserGenres
createTableQuery = ("CREATE TABLE UserGenres ("
  	"username VARCHAR(45) NOT NULL,"
  	"genre1 VARCHAR(45) NULL DEFAULT 'Fiction',"
  	"genre2 VARCHAR(45) NULL DEFAULT 'Literary Fiction/classics',"
  	"genre3 VARCHAR(45) NULL DEFAULT 'Historical',"
  	"genre4 VARCHAR(45) NULL DEFAULT 'Poetry',"
  	"genre5 VARCHAR(45) NULL DEFAULT 'Drama and Plays',"
    	"FOREIGN KEY (username) REFERENCES Users(username) on delete cascade,"
    	"PRIMARY KEY (username))")
cursor.execute(createTableQuery)

#####################################################
# Commit the data and close the connection to MySQL #
#####################################################
cnx.commit()
cnx.close()
