"""A session demonstration app."""

import calendar
from datetime import datetime
import sys
import cherrypy
from cherrypy.lib import sessions
from cherrypy._cpcompat import copyitems
import os
import os.path
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))

class Root(object):
    
    def page(self):
        changemsg = []
        if cherrypy.session.id != cherrypy.session.originalid:
            if cherrypy.session.originalid is None:
                changemsg.append('Created new session because no session id was given.')
            if cherrypy.session.missing:
                changemsg.append('Created new session due to missing (expired or malicious) session.')
            if cherrypy.session.regenerated:
                changemsg.append('Application generated a new session.')
        
        try:
            expires = cherrypy.response.cookie['session_id']['expires']
        except KeyError:
            expires = ''
        
        return env.get_template('session-tmpl.html').render(
            sessionid = cherrypy.session.id,
            changemsg = '<br>'.join(changemsg),
            respcookie = cherrypy.response.cookie.output(),
            reqcookie = cherrypy.request.cookie.output(),
            sessiondata = copyitems(cherrypy.session),
            servertime = datetime.utcnow().strftime("%Y/%m/%d %H:%M") + " UTC",
            serverunixtime = calendar.timegm(datetime.utcnow().timetuple()),
            cpversion = cherrypy.__version__,
            pyversion = sys.version,
            expires = expires,
            )
    
    def index(self):
        # Must modify data or the session will not be saved.
        cherrypy.session['user'] = 'izaguirr@gmail.com'
        return self.page()
    index.exposed = True
    
    def expire(self):
        sessions.expire()
        return self.page()
    expire.exposed = True
    
    def regen(self):
        cherrypy.session.regenerate()
        # Must modify data or the session will not be saved.
        cherrypy.session['user'] = 'izaguirr@nd.edu'
        return self.page()
    regen.exposed = True

if __name__ == '__main__':
    import os.path
    tutconf = os.path.join(os.path.dirname(__file__), 'sessions.conf')
    cherrypy.config.update({
        #'environment': 'production',
        'log.screen': True,
        'tools.sessions.on': True,
        'tools.sessions.storage_file': "file",
        'tools.sessions.storage_path': "/Users/izaguirr/sessions",
        'tools.sessions.timeout': 15,
        'cherrypy.server.ssl_module':'builtin',
        'cherrypy.server.ssl_certificate':"/etc/pki/tls/feednd.com/8e7e0752d9b41727.crt",
        'cherrypy.server.ssl_private_key':"/etc/pki/tls/feednd.com/feednd.key",
        'cherrypy.server.ssl_certificate_chain':"/etc/pki/tls/feednd.com/gd_bundle-g2-g1.crt"
        })
    cherrypy.quickstart(Root(),config=tutconf)
else:
    conf = {
        '/': {
            'tools.sessions.on' :True,
            'tools.sessions.debug' : True,
            'tools.sessions.timeout': 15,
            'log.screen': True,
            }
        }

    application = cherrypy.Application(Root(), None, conf)
