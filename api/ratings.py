''' Controller for /books/{id}
    Imported from handler for /books '''
import sys
sys.stdout = sys.stderr
import cherrypy
import threading
import os, os.path, json, logging, mysql.connector
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
#from menus import Menus
class Ratings(object):
    ''' Handles resource /books/{id} 
        Allowed methods: GET, PUT, DELETE, OPTIONS '''
    exposed = True

    def __init__(self):
	self.xtra = dict()
	self.db = dict()
        self.db['name']='shelfdata'
        self.db['user']='root'
        self.db['host']='127.0.0.1'
        self.data = []
#        self.menus=Menus()


    def GET(self, book_id):
      test = "get call for ratings"
      return test

    @cherrypy.tools.json_in(force=False)
    def POST(self, book_id):
        cnx = mysql.connector.connect(
            user=self.db['user'],
            host=self.db['host'],
            database=self.db['name'],
        )
        cursor = cnx.cursor()   
        try:
            newrating = cherrypy.request.json["rating"]
            #password = cherrypy.request.json["password"]
        except:
            return errorJSON(code=5000, message="Expected rating")

        q="select rating from Books where book_id = %s" % book_id
        cursor.execute(q)
        r=cursor.fetchall()
        if len(r) == 0:
            rating = newrating
        else:
            rating = r[0][0]
        rating = (rating + float(newrating))/2
        q="update Books set rating = %d where book_id=%s;" % (rating, book_id)

        cursor.execute(q)
        cnx.commit()
        result = {'result':'success'}
        return json.dumps(result, encoding='utf-8')


