''' /books resource for feednd.com
This is run as a WSGI application through CherryPy and Apache with mod_wsgi
Author: Alexander Hathaway and Lindsey Sansone
Date: April 19 2015
Web Applications'''
import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import math
import json
from collections import OrderedDict
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
from bookid import BookID
import logging
from config import conf
from cherrypy.lib import sessions
from ratings import Ratings
SESSION_KEY = '_cp_username'

class Books(object):
    ''' Handles resource /books
        Allowed methods: GET, POST, OPTIONS '''
    exposed = True

    def __init__(self):
        self.id=BookID()
        self.myd = dict()
        self.xtra = dict()
        self.db = dict()
        self.cat1 = dict()
        self.cat2 = dict()
        self.cat3 = dict()
        self.cat4 = dict()
        self.cat5 = dict()
        self.userGenres=[]
        self.db['name']='shelfdata'
        self.db['user']='root'
        self.db['host']='127.0.0.1'
        self.login='false'
        self.ratings=Ratings()
    def _cp_dispatch(self,vpath):
        print "Books._cp_dispatch with vpath: %s \n" % vpath
        if len(vpath) == 1: # /books/{id}
            cherrypy.request.params['book_id']=vpath.pop(0)
            return self.id
        if len(vpath) == 2: #/books/{id}/rating
            cherrypy.request.params['book_id']=vpath.pop(0)
            vpath.pop(0)
            return self.id.ratings
#      if len(vpath) == 3: #/
#            cherrypy.request.params['book_id']=vpath.pop(0)
#            vpath.pop(0) #menus
#            cherrypy.request.params['menu_id']=vpath.pop(0)
#            return self.id.menus.id 
        return vpath

    def getDataFromDB(self):
        self.login='false'
        try:
            cnx = mysql.connector.connect(
                user=self.db['user'],
                host=self.db['host'],
                database=self.db['name'],
            )
            print 'ATTEMPT to get seesion username'
            if cherrypy.session.has_key('username'):
                self.username=cherrypy.session['username']
                print self.username
                self.login='true'
            else:
                self.username='none'
                print 'NO KEY FOUND'

            cursor = cnx.cursor()
            self.userGenres = []
            if self.username != 'none':
                q="select genre1, genre2, genre3, genre4, genre5 from UserGenres where username='%s';" % self.username
                print q
                cursor.execute(q)
                for (genre1, genre2, genre3, genre4, genre5) in cursor:
                    self.userGenres=[genre1, genre2, genre3, genre4, genre5]

            else:
                 self.userGenres=['General Fiction', 'Literary Fiction/classics', 'Historical', 'Poetry', 'Drama and Plays']
            q = "select Books.book_id, title, author, imgUrl, rating from Books, Genres where genre='%s' and Books.book_id = Genres.book_id ORDER BY rating desc limit 35;" % self.userGenres[0]
            cursor.execute(q)
        except Error as e:
            logging.error(e)
            raise
        self.data = []
        self.xtra = {}
        self.cat1 = {}
        for (book_id, title, author, imgUrl, rating) in cursor:
            if imgUrl=="http://www.iblist.com/thumbs/covers/nocover.g":
                imgUrl = "images/bookcover.png"
            self.data.append({
                         'book_id':book_id,
                         'title':title,
                         'author':author,
                         'imgUrl':imgUrl,
                         'href':'books/'+str(book_id)
                         })
            self.xtra[book_id]=(title, author, imgUrl)
            self.cat1[book_id]=(title, author, imgUrl)

        q = "select Books.book_id, title, author, imgUrl from Books, Genres where genre='%s' and Books.book_id = Genres.book_id limit 25;" % self.userGenres[1]
        cursor.execute(q)
        self.cat2 = {}
        for (book_id, title, author, imgUrl) in cursor:
            if imgUrl=="http://www.iblist.com/thumbs/covers/nocover.g":
                imgUrl = "images/bookcover.png"
            self.data.append({
                         'book_id':book_id,
                         'title':title,
                         'author':author,
                         'imgUrl':imgUrl,
                         'href':'books/'+str(book_id)
                         })
            self.cat2[book_id]=(title, author, imgUrl)

        q = "select Books.book_id, title, author, imgUrl from Books, Genres where genre='%s' and Books.book_id = Genres.book_id limit 25;" % self.userGenres[2]
        cursor.execute(q)
        self.cat3 = {}
        for (book_id, title, author, imgUrl) in cursor:
            if imgUrl=="http://www.iblist.com/thumbs/covers/nocover.g":
                imgUrl = "images/bookcover.png"
            self.data.append({
                         'book_id':book_id,
                         'title':title,
                         'author':author,
                         'imgUrl':imgUrl,
                         'href':'books/'+str(book_id)
                         })
            self.cat3[book_id]=(title, author, imgUrl)

        q = "select Books.book_id, title, author, imgUrl from Books, Genres where genre='%s' and Books.book_id = Genres.book_id limit 25;" % self.userGenres[3]
        cursor.execute(q)
        self.cat4 = {}
        for (book_id, title, author, imgUrl) in cursor:
            if imgUrl=="http://www.iblist.com/thumbs/covers/nocover.g":
                imgUrl = "images/bookcover.png"
            self.data.append({
                         'book_id':book_id,
                         'title':title,
                         'author':author,
                         'imgUrl':imgUrl,
                         'href':'books/'+str(book_id)
                         })
            self.cat4[book_id]=(title, author, imgUrl)

        q = "select Books.book_id, title, author, imgUrl from Books, Genres where genre='%s' and Books.book_id = Genres.book_id limit 25;" % self.userGenres[4]
        cursor.execute(q)
        self.cat5 = {}
        for (book_id, title, author, imgUrl) in cursor:
            if imgUrl=="http://www.iblist.com/thumbs/covers/nocover.g":
                imgUrl = "images/bookcover.png"
            self.data.append({
                         'book_id':book_id,
                         'title':title,
                         'author':author,
                         'imgUrl':imgUrl,
                         'href':'books/'+str(book_id)
                         })
            self.cat5[book_id]=(title, author, imgUrl)


    @cherrypy.expose
    def GET(self):
        ''' Get list of books '''


        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        self.getDataFromDB()
        self.sd=dict()
        # Sort by closest book 
        if output_format == 'text/html':
            return env.get_template('books-tmpl.html').render(
                info=self.xtra,
                cat1=self.cat1,
                cat2=self.cat2,
                cat3=self.cat3,
                cat4=self.cat4,
                cat5=self.cat5,
                userGenres=self.userGenres,
                username=self.login,
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            return json.dumps(self.data, encoding='utf-8')

    def POST(self, **kwargs):
        ''' Add a new book '''
        result= "POST /books     ...     Books.POST\n"
        result+= "POST /books body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data; book_id should not be included
        # Insert book
        # Prepare response
        return result

    def OPTIONS(self):
        ''' Allows GET, POST, OPTIONS '''
        #Prepare response
        return "<p>/books/ allows GET, POST, and OPTIONS</p>"

class StaticAssets(object):
    pass

if __name__ == '__main__':
    conf = {
        'global': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__))
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        },
        #'/': {
        #    'tools.sessions.on':True,
        #    'tools.sessions.debug' : True,
            #'tools.sessions.storage_type': "file",
            #'tools.sessions.storage_path': "/var/www/sessions",
        #    'tools.sessions.timeout': 15,
         #}
    }
    cherrypy.tree.mount(Books(), '/books', {
        '/': {
            'request.dispatch': cherrypy.dispatch.MethodDispatcher()
        }
    })
    cherrypy.tree.mount(StaticAssets(), '/', {
        '/': {
            'tools.staticdir.root': os.path.dirname(os.path.abspath(__file__)),
        },
        '/css': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'css'
        },
        '/js': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': 'js'
        }
    })
    cherrypy.engine.start()
    cherrypy.engine.block()
else:
    application = cherrypy.Application(Books(), None, conf)

