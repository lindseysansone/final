import cherrypy
import apiutil
import mysql.connector
from mysql.connector import Error
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import json
import os
from jinja2 import Environment, FileSystemLoader
from apiutil import errorJSON
from passlib.apps import custom_app_context as pwd_context
from config import conf
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
SESSION_KEY = '_cp_username'


class UserLogin(object):
    exposed = True

    def __init__(self):
        self.db={}
        self.data={}
        self.db['name']='shelfdata'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def GET(self):
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])
        if output_format == 'text/html':
            return env.get_template('users-tmpl.html').render(
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            return json.dumps(self.data, encoding='utf-8')

    @cherrypy.tools.json_in(force=False)
    def POST(self):
        ''' login existing user
        should only be used through SSL connection
        expects JSON:
        { 'username' : username,
          'password' : password
        }
        return JSON:
        { 'errors' : [] }
        error code / message:
        5000 : Expected username and password
        5001 : Incorrect username or password
        '''
        try:
            username = cherrypy.request.json["username"]
            password = cherrypy.request.json["password"]
        except:
            return errorJSON(code=5000, message="Expected username and password")
        cnx = mysql.connector.connect(user='root',host='127.0.0.1',database='shelfdata',charset='utf8mb4')        
        cursor = cnx.cursor()
        q="select password from Users where username=%s";
        cursor.execute(q,(username,))
        r=cursor.fetchall()
        if len(r) == 0:
            return errorJSON(code=5001, message="Incorrect username or password")
        hash=r[0][0]
        match=pwd_context.verify(password,hash)
        password=""
        if not match:
            # username / hash do not exist in database
            return errorJSON(code=5001, message="Incorrect username or password")
        else:
            # username / password correct
            #cherrypy.session.regenerate()
            #print "Login SESSION KEY % s" % SESSION_KEY
            #cherrypy.session[SESSION_KEY] = cherrypy.request.login = username
            #cherrypy.session['user'] = username
            #cookie = cherrypy.response.cookie
            #cookie['username'] = username
            print 'new session made with test username'
            cherrypy.session.regenerate() 
            cherrypy.session['username'] = username
            print cherrypy.session['username']
            result={'errors':[]}
            return json.dumps(result)

application = cherrypy.Application(UserLogin(), None, conf)
