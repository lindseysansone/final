''' Controller for /books/{id}
    Imported from handler for /books '''
import sys
#sys.stdout = sys.stderr
import cherrypy
import threading
import os, os.path, json, logging, mysql.connector
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
from ratings import Ratings
class BookID(object):
    ''' Handles resource /books/{id} 
        Allowed methods: GET, PUT, DELETE, OPTIONS '''
    exposed = True

    def __init__(self):
	self.xtra = dict()
	self.db = dict()
        self.db['name']='shelfdata'
        self.db['user']='root'
        self.db['host']='127.0.0.1'
        self.data = []
        self.ratings=Ratings()
    def getDataFromDB(self, book_id):
        cnx = mysql.connector.connect(
        user=self.db['user'],
        host=self.db['host'],
        database=self.db['name'],
        )
        self.data = []
        cursor = cnx.cursor()
        q="select title, date, imgUrl, type, author, rating, summary, place, isbn from Books where book_id = '" + book_id + "';"
        cursor.execute(q)
        for (title,date,imgUrl,type1,author,rating,summary,place,isbn) in cursor:
            self.data = {'title':title,
                         'date':str(date),
                         'imgUrl':imgUrl,
                         'type':type1,
                         'author':author,
                         'rating':rating,
                         'summary':summary,
                         'place':place,
                         'href':'books/'+str(book_id),
                         'book_id': str(book_id),
			 'isbn':isbn
                         }
            #self.xtra[book_id] = ()
    

    def GET(self, book_id):
        ''' Return information on book book_id'''
        #return "GET /books/{id=%s}   ...   BookID.GET" % book_id
	

	self.getDataFromDB(book_id)
        return json.dumps(self.data, encoding='utf-8')


    def PUT(self, book_id, **kwargs):
        ''' Update book with book_id'''
        result = "PUT /books/{id=%s}      ...     BookID.PUT\n" % book_id
        result += "PUT /books body:\n"
        for key, value in kwargs.items():
            result+= "%s = %s \n" % (key,value)
        # Validate form data
        # Insert or update book
        # Prepare response
        return result

    def DELETE(self, book_id):
        ''' Delete book with book_id'''
        #Validate book_id
        #Delete book
        #Prepare response
        return "DELETE /books/{id=%s}   ...   BookID.DELETE" % book_id

    def OPTIONS(self, book_id):
        ''' Allows GET, PUT, DELETE, OPTIONS '''
        #Prepare response
        return "<p>/books/{id} allows GET and OPTIONS</p>"
