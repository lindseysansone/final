''' /books resource for feednd.com
This is run as a WSGI application through CherryPy and Apache with mod_wsgi
Author: Alexander Hathaway and Lindsey Sansone
Date: April 19 2015
Web Applications'''
import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import math
import json
from cherrypy.lib import sessions
from cherrypy._cpcompat import copyitems
from apiutil import errorJSON
from collections import OrderedDict
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf
SESSION_KEY = '_cp_username'
class UserGenres(object):
    ''' Handles resource /books
        Allowed methods: GET, POST, OPTIONS '''
    exposed = True

    def __init__(self):
        self.myd = dict()
        self.xtra = dict()
        self.db = dict()
        self.username='testman'
        self.db['name']='shelfdata'
        self.db['user']='root'
        self.db['host']='127.0.0.1'
        #try:
        #    self.cookie = cherrypy.request.cookie
        #    self.username=self.cookie['username'].value
        #except:
        #    self.username="none"

    @cherrypy.tools.json_in(force=False)
    def PUT(self):
        #try:
        #  username=str(cherrypy.request.json["username"]) 
        #  print "username received: %s" % username
        #except:
        #    print "username was not received"
        #    return errorJSON(code=9003, message="Expected text 'username' for user as JSON input")
        #try:
        #    self.cookie = cherrypy.request.cookie
        #    self.username=self.cookie['username'].value
        #except:
        #    self.username='testman'
        if cherrypy.session.has_key('username'):
            self.username=cherrypy.session['username']
            print self.username
        else:
            self.username='testman'
            print 'NO KEY FOUND'
        try:
            genre_num=str(cherrypy.request.json["genre_num"])
            print "genre received: %s" % genre_num
            
        except:
            print "genre_num was not received"
            return errorJSON(code=9003, message="Expected text 'genre_num' for user as JSON input")
        try:
            new_genre=str(cherrypy.request.json["new_genre"])
            print "new_genre received: %s" % new_genre
        except:
            print "new_genre was not received"
            return errorJSON(code=9003, message="Expected text 'new_genre' for user as JSON input")

        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'])
        cursor = cnx.cursor() 

        q="update UserGenres set %s='%s' where username='%s';" % (genre_num, new_genre, self.username)
        print q
        cursor.execute(q)
        cnx.commit()
        result={'result':'success'}
        return json.dumps(result, encoding='utf-8') 

    def OPTIONS(self):
        ''' Allows GET, POST, OPTIONS '''
        #Prepare response
        return "<p>/books/ allows GET, POST, and OPTIONS</p>"
application = cherrypy.Application(UserGenres(), None, conf)
