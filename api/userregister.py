import cherrypy
import re
import apiutil
import mysql.connector
from mysql.connector import Error
import sys
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import json
import os
from jinja2 import Environment, FileSystemLoader
from pyvalidate import validate, ValidationException
from apiutil import errorJSON
from passlib.apps import custom_app_context as pwd_context
from config import conf
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
SESSION_KEY = '_cp_username'

class UserRegister(object):
    exposed = True

    def __init__(self):
        self.db={}
        self.data={}
        self.db['name']='shelfdata'
        self.db['user']='root'
        self.db['host']='127.0.0.1'

    def GET(self):
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])
        if output_format == 'text/html':
            return env.get_template('users-register-tmpl.html').render(
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            return json.dumps(self.data, encoding='utf-8')

    @cherrypy.tools.json_in(force=False)
    def POST(self):
        try:
            username = str(cherrypy.request.json["username"])
            print "username received: %s" % username
        except:
            print "username was not received"
            return errorJSON(code=9003, message="Expected text 'username' for user as JSON input")
        try:
            email = cherrypy.request.json["email"]
            pattern = r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)"
            assert re.match(pattern, email)

            print "email received: %s" % email
        except:
            print "email was not received"
            return errorJSON(code=9003, message="Expected email 'email' for user as JSON input")
        try:
            password = cherrypy.request.json["password"]
            assert re.match(r'[A-Za-z0-9@#$%^&+=]{8,}', password)
            print "password received: %s" % password
        except:
            print "password was not received"
            return errorJSON(code=9003, message="Expected password 'password' for user as JSON input")


        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'])
        cursor = cnx.cursor()
        # Check if email already exists
        q="SELECT EXISTS(SELECT 1 FROM Users WHERE email='%s')" % email
        cursor.execute(q)
        if cursor.fetchall()[0][0]:
            #email already exists
            print "User with email %s Already Exists" % email
            return errorJSON(code=9000, message="User with email %s Already Exists") % email


        password_hash = pwd_context.encrypt(password)

        q="INSERT INTO Users (username, email, password) VALUES ('%s', '%s', '%s');" \
            % (username, email, password_hash)
        try:
            cursor.execute(q)
            #userID=cursor.fetchall()[0][0]

            q="insert into UserGenres (username) VALUES ('%s')" % username
            cursor.execute(q)
            cnx.commit()
            cnx.close()
        except Error as e:
            #Failed to insert user
            print "mysql error: %s" % e
            return errorJSON(code=9002, message="Failed to add user")
        result = {'username':username, 'email':email, 'errors':[]}
        return json.dumps(result)
application = cherrypy.Application(UserRegister(), None, conf)
