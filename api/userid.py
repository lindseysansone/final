''' /books resource for feednd.com
This is run as a WSGI application through CherryPy and Apache with mod_wsgi
Author: Alexander Hathaway and Lindsey Sansone
Date: April 19 2015
Web Applications'''
import apiutil
import sys
import os.path
sys.stdout = sys.stderr # Turn off console output; it will get logged by Apache
import threading
import cherrypy
import os
import os.path
import math
import json
from cherrypy.lib import sessions
from cherrypy._cpcompat import copyitems
from collections import OrderedDict
import mysql.connector
from mysql.connector import Error
from jinja2 import Environment, FileSystemLoader
env = Environment(loader=FileSystemLoader(os.path.abspath(os.path.dirname(__file__))+'/templates/'))
import logging
from config import conf
SESSION_KEY = '_cp_username'
class UserID(object):
    ''' Handles resource /books
        Allowed methods: GET, POST, OPTIONS '''
    exposed = True

    def __init__(self):
        self.myd = dict()
        self.xtra = dict()
        self.db = dict()
        self.genres=[]
        self.userGenres=[]
        self.reading = dict()
        self.willread = dict()
        self.read = dict()
        self.genres=['Fiction', 'Humor', 'Science Fiction', 'Literary Fiction/classics', 'Drama and Plays', 'Historical', 'Epic', 'Poetry', 'Romance']
        self.username='testman'
        self.db['name']='shelfdata'
        self.db['user']='root'
        self.db['host']='127.0.0.1'
        #try:
        #    self.cookie = cherrypy.request.cookie
        #    self.username=self.cookie['username'].value
        #except:
        #    self.username="none"
          
        self.username='testman'
    def getDataFromDB(self):
        #print 'session info'
        #self.cookie = cherrypy.request.cookie
        #self.username=self.cookie['username'].value
        #cookie = cherrypy.request.cookie
        #print 'cookie:'
        #print cookie
        if cherrypy.session.has_key('username'):
            self.username=cherrypy.session['username']
            print self.username
        else:
            self.username='none'
            print 'NO KEY FOUND'


        try:
            cnx = mysql.connector.connect(
                user=self.db['user'],
                host=self.db['host'],
                database=self.db['name'],
            )
            cursor = cnx.cursor()
            q = "select Books.book_id, title, author, imgUrl from Books, UserLibrary where Books.book_id = UserLibrary.book_id and username='%s' and status='reading';" % self.username
            cursor.execute(q)
        except Error as e:
            logging.error(e)
            raise
        self.data = []
        self.xtra = {}
        self.reading = {}
        for (book_id, title, author, imgUrl) in cursor:
            if imgUrl=="http://www.iblist.com/thumbs/covers/nocover.g":
                imgUrl = "images/bookcover.png"
            self.data.append({
                         'book_id':book_id,
                         'title':title,
                         'author':author,
                         'imgUrl':imgUrl,
                         'href':'books/'+str(book_id)
                         })
            self.xtra[book_id]=(title, author, imgUrl)
            self.reading[book_id]=(title, author, imgUrl)

        q = "select Books.book_id, title, author, imgUrl from Books, UserLibrary where Books.book_id = UserLibrary.book_id and username='%s' and status='will_read';" % self.username
        cursor.execute(q)
        self.willread = {}
        for (book_id, title, author, imgUrl) in cursor:
            if imgUrl=="http://www.iblist.com/thumbs/covers/nocover.g":
                imgUrl = "images/bookcover.png"
            self.data.append({
                         'book_id':book_id,
                         'title':title,
                         'author':author,
                         'imgUrl':imgUrl,
                         'href':'books/'+str(book_id)
                         })
            self.willread[book_id]=(title, author, imgUrl)

        q = "select Books.book_id, title, author, imgUrl from Books, UserLibrary where Books.book_id = UserLibrary.book_id and username='%s' and status='read';" % self.username
        cursor.execute(q)
        self.read = {}
        for (book_id, title, author, imgUrl) in cursor:
            if imgUrl=="http://www.iblist.com/thumbs/covers/nocover.g":
                imgUrl = "images/bookcover.png"
            self.data.append({
                         'book_id':book_id,
                         'title':title,
                         'author':author,
                         'imgUrl':imgUrl,
                         'href':'books/'+str(book_id)
                         })
            self.read[book_id]=(title, author, imgUrl)

        q = "select distinct genre from Genres;"
        self.genres = []
        cursor.execute(q)
        for genre in cursor:
            self.genres.append(genre[0])
        
        q="select genre1, genre2, genre3, genre4, genre5 from UserGenres where username='%s';" % self.username
        self.userGenres=[]
        cursor.execute(q)
        for (genre1, genre2, genre3, genre4, genre5) in cursor:
            self.userGenres=[genre1, genre2, genre3, genre4, genre5]



        print 'will read'
        print self.willread
    def GET(self):
        ''' Get list of books '''

        # Return data in the format requested in the Accept header
        # Fail with a status of 406 Not Acceptable if not HTML or JSON
        output_format = cherrypy.lib.cptools.accept(['text/html', 'application/json'])

        self.getDataFromDB()
        self.sd=dict()
        # Sort by closest book 
        if output_format == 'text/html':
            return env.get_template('userid-tmpl.html').render(
                info=self.xtra,
                reading=self.reading,
                willread=self.willread,
                read=self.read,
                genres=self.genres,
                userGenres=self.userGenres,
                base=cherrypy.request.base.rstrip('/') + '/'
            )
        else:
            return json.dumps(self.data, encoding='utf-8')

    @cherrypy.tools.json_in(force=False)
    def POST(self):
    #ADD BOOK TO USERS BOOK SHELF WITH STATUS
        #try:
        #  username=str(cherrypy.request.json["username"]) 
        #  print "username received: %s" % username
        #except:
        #    print "username was not received"
        #    return errorJSON(code=9003, message="Expected text 'username' for user as JSON input")
        #try:
        #    self.cookie = cherrypy.request.cookie
        #    self.username=self.cookie['username'].value
        #except:
        #    self.username='non'
        if cherrypy.session.has_key('username'):
            self.username=cherrypy.session['username']
            print self.username
        else:
            self.username='testman'
            print 'NO KEY FOUND'
        try:
            book_id=str(cherrypy.request.json["book_id"])
            print "book_id received: %s" % book_id
            book_id = int(book_id)
        except:
            print "book_id was not received"
            return errorJSON(code=9003, message="Expected text 'book_id' for user as JSON input")
        try:
            status=str(cherrypy.request.json["status"])
            print "status received: %s" % status
        except:
            print "status was not received"
            return errorJSON(code=9003, message="Expected text 'status' for user as JSON input")

        cnx = mysql.connector.connect(user=self.db['user'],host=self.db['host'],database=self.db['name'])
        cursor = cnx.cursor() 
        q="select * from UserLibrary where book_id=%d" % book_id
        cursor.execute(q)
        if cursor.fetchall(): #user already has book on bookshelf
            q="update UserLibrary set status='%s' where username='%s' and book_id=%d" % (status, self.username, book_id)
            print q
            cursor.execute(q)
        else: #insert new entry into users bookshelf
            q="insert into UserLibrary (book_id, status, username) VALUES (%d, '%s', '%s')" % (book_id, status, self.username)
            print q
            cursor.execute(q)
        cnx.commit()
        result={'result':'success'}
        return json.dumps(result, encoding='utf-8') 

    def OPTIONS(self):
        ''' Allows GET, POST, OPTIONS '''
        #Prepare response
        return "<p>/books/ allows GET, POST, and OPTIONS</p>"
application = cherrypy.Application(UserID(), None, conf)
