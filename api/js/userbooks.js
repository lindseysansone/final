
function imgError(image) {
    image.onerror = "";
    image.src = "http://www.alexanderwhathaway.com/images/bookcover.png";
    image.width="80";
    image.height="100";
    return true;
}

$(document).ready(function(){


function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    console.log(ca);
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}

  //load book details in modal
  $(".box").click(function(){
    var book = $(this);
    var book_id = $(this).find('p.book_id').text();
    
    $.ajax({
      type: 'GET',
      url: "/books/" + book_id,
      dataType:'json',
      success: function(data) {
        console.log(data);
        var html = "";
        html = html + "<span><div style='display:inline-block'><h2 id='module-book-id' name=" + data['book_id']+">" + data['title'] + "</h2>";
        html = html + "<p>By: " + data['author'] + "</p>";
        html = html + "<p>" + data['place'] + "</p>";
        html = html + "<p>Rating: " + data['rating'] + "</p></div>";
        html = html + "<div style='display:inline-block; float:right'><img src='" + data['imgUrl'] + "' onerror='imgError(this);' width:'80'></div> </span>";
        html = html + "<br><span style='clear:both;'><select style='display:inline-block;' id='bookshelf-select'>";
        html = html + "<option value='reading'>Reading</option>";
        html = html + "<option value='will_read' selected>Want to Read</option>";
        html = html + "<option value='read'>Already Read</option>";
        html = html + "</select>";
        html = html + "<button type='button' style=' display:inline-block;' id='add-bookshelf-btn'>Move on Bookshelf</button></span>";

        html = html + "<br><span style='clear:both;'><select style='display:inline-block; width:104px' id='rating-select'>";
        html = html + "<option value='1'>1</option>";
        html = html + "<option value='2'>2</option>";
        html = html + "<option value='3'>3</option>";
        html = html + "<option value='4'>4</option>";
        html = html + "<option value='5'>5</option>";
        html = html + "<option value='6'>6</option>";
        html = html + "<option value='7'>7</option>";
        html = html + "<option value='8'>8</option>";
        html = html + "<option value='9'>9</option>";
        html = html + "<option value='10' selected>10</option>";
        html = html + "</select>";
        html = html + "<button type='button' style=' display:inline-block; width:106px' id='rate-book-btn'>Rate Book</button></span>";


        html = html + "<p>" + data['summary'] + "</p>";
        
        $('.book-info').html(html);
        $('.modalDialog').css({"opacity":"1", "pointer-events":"auto"});
      }
    }); 
    
  });
  //Close book detail modal
  $('.close').click(function(event){
    event.preventDefault();
    $('.modalDialog').css({"opacity":"0", "pointer-events":"none"});
  });


  $('#account-link').click(function(event){
    event.preventDefault();
    //var username = getCookie('username');
    var username = $('#username').text();
    console.log(username);
    if(username && username!='false'){
      window.location = '/users/userid';      
    }
    else{
      window.location = this.href;
    }
  });

  $(document.body).on('click', '#add-bookshelf-btn', function(){
    var bookStatus = $('#bookshelf-select').val();
    var bookid= $('#module-book-id').attr('name');
    var data = {'status':bookStatus, 'book_id':bookid };

    $.ajax({
      type: 'POST',
      contentType: 'application/json',
      url: "/users/userid",
      data: JSON.stringify(data),
      dataType:'json',
      success: function() {
        console.log('book added to bookshelf');
        $('.modalDialog').css({"opacity":"0", "pointer-events":"none"});
        location.reload();
      }
    });

  });


  $('#genre1-select').change(function() {
    var newVal = $('#genre1-select').val();
    var data = {'genre_num':'genre1', 'new_genre':newVal};
    $.ajax({
      type: 'PUT',
      contentType: 'application/json',
      url: "/users/genres",
      data: JSON.stringify(data),
      dataType:'json',
      success: function() {
        console.log('fav user genre changed');
        
      },
    });

  });

  $('#genre5-select').change(function() {
    var newVal = $('#genre5-select').val();
    var data = {'genre_num':'genre5', 'new_genre':newVal};
    $.ajax({
      type: 'PUT',
      contentType: 'application/json',
      url: "/users/genres",
      data: JSON.stringify(data),
      dataType:'json',
      success: function() {
        console.log('fav user genre changed');

      },
    });

  });

  $('#genre2-select').change(function() {
    var newVal = $('#genre2-select').val();
    var data = {'genre_num':'genre2', 'new_genre':newVal};
    $.ajax({
      type: 'PUT',
      contentType: 'application/json',
      url: "/users/genres",
      data: JSON.stringify(data),
      dataType:'json',
      success: function() {
        console.log('fav user genre changed');

      },
    });

  });

  $('#genre3-select').change(function() {
    var newVal = $('#genre3-select').val();
    var data = {'genre_num':'genre3', 'new_genre':newVal};
    $.ajax({
      type: 'PUT',
      contentType: 'application/json',
      url: "/users/genres",
      data: JSON.stringify(data),
      dataType:'json',
      success: function() {
        console.log('fav user genre changed');

      },
    });

  });

  $('#genre4-select').change(function() {
    var newVal = $('#genre4-select').val();
    var data = {'genre_num':'genre4', 'new_genre':newVal};
    $.ajax({
      type: 'PUT',
      contentType: 'application/json',
      url: "/users/genres",
      data: JSON.stringify(data),
      dataType:'json',
      success: function() {
        console.log('fav user genre changed');

      },
    });

  });

  $('#logout-btn').click(function() {
    $.ajax({
      type: 'POST',
      contentType: 'application/json',
      url: "/users/logout",
      //dataType:'json',
      success: function() {
        console.log('successfully logged out');
        window.location = "/users/login"
      },
    });

  });

  $(document.body).on('click', '#rate-book-btn', function(){
    var rating = $('#rating-select').val();
    var bookid= $('#module-book-id').attr('name');
    var data = {'rating':rating};

    $.ajax({
      type: 'POST',
      contentType: 'application/json',
      url: "/books/" + bookid + "/ratings",
      data: JSON.stringify(data),
      success: function() {
        console.log('successfully rated book');
        $('.modalDialog').css({"opacity":"0", "pointer-events":"none"});
      }
    });

  });

});


