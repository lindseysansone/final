
function checkForm(form)
{
  console.log('checkform');
  if(form.username.value == "") {
      alert("Error: Username cannot be blank!");
      form.username.focus();
      return false;
    }

  if(form.password.value != "" && form.password.value == form.password2.value) {
      if(form.password.value.length < 6) {
        alert("Error: Password must contain at least six characters!");
        form.password.focus();
        return false;
      }
      if(form.password.value == form.username.value) {
        alert("Error: Password must be different from Username!");
        form.password.focus();
        return false;
      }
      re = /[0-9]/;
      if(!re.test(form.password.value)) {
        alert("Error: password must contain at least one number (0-9)!");
        form.password.focus();
        return false;
      }
      re = /[a-z]/;
      if(!re.test(form.password.value)) {
        alert("Error: password must contain at least one lowercase letter (a-z)!");
        form.password.focus();
        return false;
      }
      re = /[A-Z]/;
      if(!re.test(form.password.value)) {
        alert("Error: password must contain at least one uppercase letter (A-Z)!");
        form.password.focus();
        return false;
      }
    } else {
      alert("Error: Please check that you're passwords match!");
      form.password.focus();
      return false;
    }

  re = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
  if(!re.test(form.email.value)) {
    alert("Error: Please enter a valid email address");
    form.email.focus();
    return false;
  }
   
  return true;
  
}
$(document).ready(function(){
  $('#register-user-form-1').submit(function(event) {
    event.preventDefault();
    console.log('submit user');
    var username = $('#username-input').val();
    var password = $('#password-input').val();
    var email = $('#email-input').val();

    $.ajax({
      type: 'POST',
      url: 'register/',
      contentType: 'application/json',
      data: JSON.stringify({
        username:username,
        password:password,
        email:email
      }),
      dataType:'json'
      
    }).success(function() {
      alert("User successfully added");
      window.location = "/users/login"
    });

  });
});
