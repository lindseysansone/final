$(document).ready(function(){
  $('#register-btn').click(function(event){
    event.preventDefault();
    console.log('here');
    window.location = '/users/register';
  });


  $('#login-btn').click(function(event) {
    event.preventDefault();
    console.log('submit user');
    var username = $('#username-input').val();
    var password = $('#pwd-input').val();

    $.ajax({
      type: 'POST',
      url: '/users/login',
      contentType: 'application/json',
      data: JSON.stringify({
        username:username,
        password:password,
      }),
      dataType:'json'

    }).success(function(data) {
      console.log(data)
      if (data['errors'].length==0){
        console.log('successful login');
        window.location = '/users/userid';
      }
      else {
        console.log('unsuccessful login');
      }

    });

  });

});
