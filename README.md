# README #

When you walk into a book store, you’re often flooded with tons of books with flashy covers or you may have a list of books that you have always wanted to read but never had the chance; our application will be a simple way to track the books you want to read so you don’t let the overwhelming nature of picking a book keep you from actually doing it.   We’re aiming to be a Netflix for books; we hope to allow users to browse books and find recommendations based on their interests.  Our application will be a simple interface that won’t overwhelm the user by pulling them in too many directions at once.

Version 0.1

### In Development ###
Currently building the back end for the site. 

### How do I get set up? - TODO ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* If you wish to contribute, please contact us and we would be happy to incorporate changes. 

### Who do I talk to? ###

* Lindsey Sansone - lsansone@nd.edu
* Alexander Hathaway